#LattePanda에서 실행되는 Client 프로그램 (스마트 공기청정기)

NodeJS로 작성되었으며, Socket.io-client 라이브러리 이용하여 통신.


내장된 아두이노와 Serial 통신으로 전달되는 실시간 데이터를 받아 Thing-Server로 전송.


Thing-Server에서 받은 값으로 팬 제어 명령을 Serial 통신으로 전달.


통신은 서버측에서 SSL 이용
