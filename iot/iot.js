/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const io = require("socket.io-client");
const AWS = require("aws-sdk");
const AWSIoTConnectionInfo = require("./configs/aws/AWSIoTConnectionInfo");
const {subscribes, publishes} = require("./configs/aws/ThingTopics");
const DTHShadow = require("./configs/aws/DTHShadow");
const config = require("./configs/config");
const getThingInfo = require("./helpers/getThingInfo");
const ConnectSP_DTH = require("./ConnectSP_DTH");
const Thing_DTH = require("./Thing_DTH");
const ThingCreator = require("./helpers/ThingCreator");
const AWSConfig = require("./configs/aws/AWSConfig");


//AWS IoT에 사물 등록 - 반복 등록 무시됌
let iot = new AWS.Iot(AWSConfig.aws);
let thingCreator = new ThingCreator(config.thing.name, AWSConfig.thingGroup);
thingCreator.registerThing(iot)
    .then(result => {
        console.log("AWS connected");
        // 처음 켜면 thing-server 에 사물 create 요청 후 thing 리턴
        // thing 정보 파일에 저장
        // 이후 켤 때마다 id 값을 clientToken 으로 사용
        getThingInfo().then(thingInfo=> {
            let socket = undefined;
            let thing_DTH = undefined;

            let clientToken = thingInfo._id;

            if (config.thing.transfer_mode === "socket") {
                socket = connectSocket(thingInfo.token);

                /**
                 * @type {{power: boolean}}
                 */
                socket.on("power", (data) => {
                    if (connectSP_DTH && connectSP_DTH.isOpen) {
                        if (data.power) connectSP_DTH.turnOn();
                        else connectSP_DTH.turnOff();
                    } else {
                        let error = new Error("socket: 기기와 통신 불가");
                        console.error(error);
                        let json = {
                            error: error.message
                        };
                        socket.emit("power", json);
                    }
                });
            } else {
                thing_DTH = connectAWS();
                thing_DTH.clientToken = clientToken;
                thing_DTH.onMessage = (topic, payload) => {
                    if (subscribes(config.thing.name)[3] === topic) { // .../update/delta
                        let delta = JSON.parse(payload.toString());
                        if (connectSP_DTH && connectSP_DTH.isOpen) {
                            if (delta.state.power) connectSP_DTH.turnOn();
                            else connectSP_DTH.turnOff();
                        } else console.error(new Error("AWS IoT: 기기와 통신 불가"));
                    }
                };
            }
            let shadow = DTHShadow;

            let connectSP_DTH = getDTH(result => {
                if (config.thing.transfer_mode === "socket") {
                    if (socket && socket.connected) {
                        shadow.state.reported = result;
                        shadow.clientToken = clientToken;
                        socket.emit("realTimeDTH", shadow);
                        console.log("WebSocket 으로 현재 값 전송");
                    } else if (socket) {
                        console.log("thing-server 연결 실패");
                        console.log("thing-server 재연결 시도");
                        socket.open();
                    }
                } else {
                    if (thing_DTH && thing_DTH.isConnected) {
                        thing_DTH.shadow.state.reported = result;
                        thing_DTH.publish(publishes(config.thing.name)[0], thing_DTH.shadow);
                        console.log("AWS IoT를 통해 현재 값 전송");
                    }
                }
            });

        }).catch(error => console.error("고객센터에 문의 필요, Error:", error));
    })
    .catch(console.error);

// 센서로부터 상태 값을 받아서,
// 1. AWS IoT or WebSocket(thing_api)으로 전송
// 2. 전원 컨트롤 명령을 받으면 사물의 전원을 제어

/**
 * @callback DTHCallback
 * @param {{temperature: number, humidity: number, pm2_5: number, pm10: number, power: boolean}} result
 */
/**
 *
 * @param {DTHCallback} callback ({temperature: number, humidity: number, pm2_5: number, pm10: number, power: boolean})
 * @return {ConnectSP_DTH}
 * @constructor
 */
function DTHFromSP(callback) {
    const success = (portName) => {
        console.log("ConnectSP_DTH: " + portName + " connected");
        connectSP_DTH.onReceiveDTH(result => {
            callback(result);
            // 농도가 낮아지면 자동 Off
            if (result.pm2_5 <= 16 && result.pm10 <= 30) {
                connectSP_DTH.turnOff();
            }
            // 농도가 높아지면 자동 On
            // if (result.pm2_5 > 35 && result.pm10 > 80) {
            //     connectSP_DTH.turnOff();
            // }
        });
    };

    let connectSP_DTH = new ConnectSP_DTH();
    connectSP_DTH.debug = true;
    connectSP_DTH.connect("COM5")
        .catch(err => {
            let port = "COM3";
            console.log("Error opening port: ", err.message);
            console.log("Retry opening port: " + port);
            return connectSP_DTH.connect(port)
        })
        .then(success)
        .catch(err => {
            console.log("Error opening port: ", err.message);
        });

    return connectSP_DTH;
}

/**
 *
 * @param {DTHCallback} callback ({temperature: number, humidity: number, pm2_5: number, pm10: number, power: boolean})
 * @return {ConnectSP_DTH}
 */
function getDTH(callback) {
    if (config.thing.debug) {
        setInterval(() => {
            let result = {
                temperature: 0,
                humidity: 0,
                pm2_5: 0,
                pm10: 0,
                power: true
            };
            callback(result);
        }, 2000);
    } else {
        return DTHFromSP(callback);
    }
}

/**
 *
 * @param token
 * @return {socket}
 */
function connectSocket(token) {
    let socket = io(config.api.thing_server, {
        query: {
            // tokens: ["eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YjcwMDhjYzllMTBkNDA5OGNlZDNkYzQiLCJuYW1lIjoibGF0dGVfMiIsImtleSI6IkVTRC1BU0RGIiwiaWF0IjoxNTM0MDY4OTQwLCJpc3MiOiJtaXRhbCIsInN1YiI6InRoaW5nSW5mbyJ9.EwOrjQiUNU-mh9U9QU3sjnV7lc9f6vRUdxkS16-kRks"],
            tokens: [token],
            from: "thing"
        }
    });
    socket.on("connect", () => {
        console.log("thing-server connected");
    });
    socket.on("power", (data) => {
        console.log(data);
        // socket.disconnect();
    });
    /**
     * @type {{name: string, line: number}} data
     */
    socket.on("predictDTH", (data) => {
        data.name = config.thing.name;
        socket.emit("predictDTH", data);
        console.log("predictDTH 길이 설정: " + data.line);
    });
    socket.on("error", (reason) => {
        console.log("err: ", reason.message);
        socket.disconnect();
    });
    socket.on("disconnect", (reason) => {
        console.log("disconnect, reason: ", reason);
    });
    return socket;
}

/**
 *
 * @return {Thing_DTH}
 */
function connectAWS() {
    let thing_DTH = new Thing_DTH();
    thing_DTH.connect(AWSIoTConnectionInfo)
        .then(() => {
            console.log("AWS IoT connected");
            thing_DTH.subscribe(subscribes(config.thing.name));
        })
        .catch((err) => {
            console.log("AWS IoT connection failed", err);
        });
    return thing_DTH;
}


