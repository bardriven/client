/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

// 사물 추가 제거 (필요한 그룹, 보안, 타입 작업 포함)
class Thing_DTH_Creator {
    /**
     *
     * @param {string} thingName
     * @param {{thingTypeName: string, thingGroupName: string, thingGroupArn: string, principal: string}} thingInfo
     */
    constructor(thingName, thingInfo) {
        this._name = thingName;
        this._arn = undefined;
        this._type = thingInfo.thingTypeName;
        this._group = thingInfo.thingGroupName;
        this._groupArn = thingInfo.thingGroupArn;
        this._principal = thingInfo.principal;
    }


    // 사물 초기화 (등록)
    registerThing(iot) {
        const createThingOpt = {
            thingName: this._name,
            thingTypeName: this._type
        };
        const addThingToThingGroupOpt = {
            thingName: this._name,
            thingArn: this._arn,
            thingGroupName: this._group,
            thingGroupArn: this._groupArn
        };
        const attachThingPrincipalOpt = {
            thingName: this._name,
            principal: this._principal
        };

        const addThingToThingGroup = result => {
            if (result) {
                this._arn = result.thingArn;
                return iot.addThingToThingGroup(addThingToThingGroupOpt).promise();
            }
        };

        const attachThingPrincipal = _ => {
            return iot.attachThingPrincipal(attachThingPrincipalOpt).promise();
        };

        const respond = () => {
            return {
                name: this._name,
                arn: this._arn,
                type: this._type,
                group: this._group,
                groupArn: this._groupArn,
                principal: this._principal,
            };
        };

        return iot.createThing(createThingOpt).promise()
            .then(addThingToThingGroup)
            .then(attachThingPrincipal)
            .then(respond);

        //
        // return this.createThing(iot, this._name, this._type).then(value => {
        //     if (value.thingArn) {
        //         this._arn = value.thingArn;
        //         return this.addThingToThingGroup(iot, this._name, this._arn, this._group, this._groupArn);
        //     }
        // }).then(() => {
        //     return this.attachThingPrincipal(iot, this._name, this._principal);
        // }).then(() => {
        //     return this;
        // }).catch(reason => {
        //     return reason;
        // });
    }
    //
    // /**
    //  *
    //  * @param iot
    //  * @param thingName
    //  * @param thingTypeName
    //  * @returns {Promise<PromiseResult<Iot.CreateThingResponse, AWSError>>}
    //  */
    // // 사물 생성
    // createThing(iot, thingName, thingTypeName) {
    //     return iot.createThing({
    //         thingName: thingName,
    //         thingTypeName: thingTypeName
    //     }).promise();
    // }
    //
    // /**
    //  *
    //  * @param iot
    //  * @param thingName
    //  * @param thingArn
    //  * @param thingGroupName
    //  * @param thingGroupArn
    //  * @returns {Promise<PromiseResult<Iot.AddThingToThingGroupResponse, AWSError>>}
    //  */
    // // 사물을 그룹에 포함
    // addThingToThingGroup(iot, thingName, thingArn, thingGroupName, thingGroupArn) {
    //     return iot.addThingToThingGroup({
    //         thingName: thingName,
    //         thingArn: thingArn,
    //         thingGroupName: thingGroupName,
    //         thingGroupArn: thingGroupArn
    //     }).promise();
    // }
    //
    // /**
    //  *
    //  * @param iot
    //  * @param thingName
    //  * @param principal
    //  * @returns {Promise<PromiseResult<Iot.AttachThingPrincipalResponse, AWSError>>}
    //  */
    // // 사물에 인증서 적용
    // attachThingPrincipal(iot, thingName, principal) {
    //     return iot.attachThingPrincipal({
    //         thingName: thingName,
    //         principal: principal
    //     }).promise();
    // }
    //
    // /**
    //  *
    //  * @param iot
    //  * @param thingName
    //  * @returns {Promise<PromiseResult<Iot.DeleteThingResponse, AWSError>>}
    //  * @private
    //  */
    // // 사물 삭제
    // deleteThing(iot, thingName) {
    //     return iot.deleteThing({
    //         thingName: thingName
    //     }).promise();
    // }
    //
    // /**
    //  *
    //  * @param iot
    //  * @param thingName
    //  * @param principal
    //  * @returns {Promise<PromiseResult<Iot.DetachThingPrincipalResponse, AWSError>>}
    //  */
    // // 사물에 인증서 삭제
    // detachThingPrincipal(iot, thingName, principal) {
    //     return iot.detachThingPrincipal({
    //         thingName: thingName,
    //         principal: principal
    //     }).promise();
    // }
}

module.exports = Thing_DTH_Creator;