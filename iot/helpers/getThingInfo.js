/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const fs = require("fs");
const request = require("request");
const config = require("../configs/config");


let path = __dirname + "/../configs/thingInfo.json";
// thingInfo 파일이 있는 지 체크
// 없으면 등록 요청, 있으면 로드
// 등록 요청 리턴 값으로 thingInfo 생성
// 에러 리턴시 409면 조회, 다른거면 프로그램 종료
// 생성된 thingInfo 로드

// output: thingInfo

function respond(data) {
    return JSON.parse(data.toString());
}

function loadJSON() {
    return new Promise((resolve, reject) => {
        fs.readFile(path, (err, data) => {
            if (err) reject(err);
            // console.log(err, data);
            resolve(data);
        });
    });
}


function getThingInfo(error) {
    if (error.code === "ENOENT") {
        console.log("사물 등록 중...");
        let path = "/things";
        let requestOptions = {
            url: config.api.api_server + path + "?apikey=" + config.api.APIKey,
            method: "POST",
            json: {
                name: config.thing.name,
                key: config.thing.key
            },
            strictSSL: config.api.strictSSL
        };

        return new Promise((resolve, reject) => {
            request(requestOptions, (err, response, result) => {
                if (err)
                    reject(err);
                if (response) {
                    if (response.statusCode === 200 && result.success) {
                        console.log("사물 등록 완료");
                        resolve(result.data);
                    } else if (response.statusCode === 409 && !result.success) {
                        console.log("이미 등록된 사물");
                        resolve();
                    } else reject(new Error(response.statusCode));
                } else reject(new Error("알 수 없는 에러"));
            });
        });
    } else return Promise.reject(error);
}

function lookupThingToken(thing) {
    console.log("등록된 사물 토큰 조회 요청 중...");
    let path = "/things/auth/token";
    let requestOptions = {
        url: config.api.api_server + path + "?apikey=" + config.api.APIKey,
        method: "POST",
        json: {
            name: config.thing.name,
            key: config.thing.key
        },
        strictSSL: config.api.strictSSL
    };

    return new Promise((resolve, reject) => {
        request(requestOptions, (err, response, result) => {
            if (err) reject(err);
            if (response.statusCode === 200 && result.success) {
                if (thing) {
                    thing.token = result.data;
                    resolve(thing);
                } else resolve(result.data);
            } else {
                // console.log(result.message, response.statusCode);
                reject(new Error(response.statusCode));
            }
        })
    });

}
function lookupThingInfo(data) {
    if (data.constructor.name === "Object") return Promise.resolve(data);

    let token = data;
    let path = "/things/auth/me" + "?thingtoken=" + token;
    let requestOptions = {
        url: config.api.api_server + path + "&apikey=" + config.api.APIKey,
        method: "GET",
        json: {},
        strictSSL: config.api.strictSSL
    };

    return new Promise((resolve, reject) => {
        request(requestOptions, (err, response, result) => {
            if (err) reject(err);
            if (response.statusCode === 200 && result.success) {
                result.data.token = token;
                resolve(result.data);
            } else {
                // console.log(result.message, response.statusCode);
                reject(new Error(response.statusCode));
            }
        })
    });
}

function recover(error) {
    return getThingInfo(error)
        .then(lookupThingToken)
        .then(lookupThingInfo)
        .then(saveJsFile)
        .then(loadJSON);
}

function saveJsFile(thingInfo) {
    if (thingInfo.constructor.name === "Buffer") return Promise.resolve();
    else {
        return new Promise((resolve, reject) => {
            let data = JSON.stringify(thingInfo);
            fs.writeFile(path, data, { flag: "w" }, (err) => {
                if (err) return reject(err);
                console.log("등록 정보 저장 완료");
                resolve();
            });
        })
    }
}

/**
 *
 * @return {Promise<object | Error>}
 */
module.exports = () => {
    return loadJSON()
        .catch(recover)
        .then(respond)
};