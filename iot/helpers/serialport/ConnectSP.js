/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const SerialPort = require('serialport');

class ConnectSP {
    get isOpen() {
        return this._isOpen;
    }
    /**
     *
     * @param {string} portName
     * @param {number} baudRate
     */
    constructor(portName, baudRate) {
        this._portName = portName;
        this._isOpen = false;
        this._port = new SerialPort(portName, {
            autoOpen: false,
            baudRate: baudRate
        });
    }

    /**
     *
     * @returns {Promise<>}
     */
    connect() {
        return new Promise((resolve, reject) => {
            this._port.open((err) => {
                if (err) {
                    reject(err);
                    //return console.log('Error opening port: ', err.message);
                }
                this._isOpen = true;
                resolve(this._portName);
            });
        });
    }

    /**
     * @callback bufferCallback
     * @param {Buffer} result
     */
    /**
     *
     * @param {bufferCallback} callback (Buffer)
     */
    onReceive(callback) {
        this._port.on('data', callback);
    }

    /**
     * @callback callback
     */
    /**
     *
     * @param {string} data
     * @param {string} [encoding ]
     * @param {callback} [callback]
     */
    write(data, encoding, callback) {
        this._port.write(data, encoding, callback);
    }


    /**
     *
     * @returns {Promise<>}
     */
    disconnect() {
        return new Promise((resolve) => {
            this._port.close(() => {
                this._isOpen =false;
                resolve();
            });
        });
    }
}

module.exports = ConnectSP;