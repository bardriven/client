/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

/**
 *
 * @type {{debug: boolean, name: string, key: string, transfer_mode:string}}
 */
module.exports.thing = {
    debug: false,
    name: "latte_1", // todo 사물 이름은 유일해야함
    key: "ASD-ASDF", // todo 사물 키는 유일해야함
    transfer_mode: "socket" // aws | socket
};

/**
 *
 * @type {{APIKey: string, api_server: string, thing_server: string, strictSSL: boolean}}
 */
module.exports.api = {
    APIKey: "APIKeyAPIKey",
    api_server: "https://dev.osirptpm.ml:8091",
    thing_server: "https://dev.osirptpm.ml:8092",
    strictSSL: true
};