/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const ConnectSP = require("./helpers/serialport/ConnectSP");

class ConnectSP_DTH {
    /**
     *
     * @return {boolean}
     */
    get debug() {
        return this._debug;
    }
    /**
     *
     * @param {boolean} value
     */
    set debug(value) {
        this._debug = value;
    }

    get isOpen() {
        return this._dth.isOpen;
    }

    constructor() {
        this._power = false;
        this._debug = false;
    }

    /**
     *
     * @param {string} portName
     * @returns {Promise<>}
     */
    connect(portName) {
        this._portName = portName;
        this._dth = new ConnectSP(this._portName, 9600);
        return this._dth.connect();
    }


    /**
     * @callback DTHCallback
     * @param {{temperature: number, humidity: number, pm2_5: number, pm10: number, power: boolean}} result
     */
    /**
     *
     * @param {DTHCallback} callback ({temperature: number, humidity: number, pm2_5: number, pm10: number, power: boolean})
     */
    onReceiveDTH(callback) {
        if (!this._debug) {
            this._dth.onReceive((data) => {
                let temp = data.toString().split(",");
                if (temp.length !== 5) return;
                callback({
                    temperature: parseFloat(temp[0]),
                    humidity: parseFloat(temp[1]),
                    pm2_5: parseFloat(temp[2]),
                    pm10: parseFloat(temp[3]),
                    power: (parseInt(temp[4]) === 1)
                });
                this._power = (parseInt(temp[4]) === 1);
            });
        } else {
            this._dth.onReceive((data) => {
                let checksum = new Buffer.alloc(1);
                for (let i = 2; i < 8; i++) {
                    checksum[0] += data[i];
                }
                if (data[8] === checksum[0]) {
                    let pm2_5 = (data[3] * 256 + data[2]) / 10;
                    let pm10 = (data[5] * 256 + data[4]) / 10;
                    callback({
                        temperature: 25,
                        humidity: 50,
                        pm2_5: parseFloat(pm2_5),
                        pm10: parseFloat(pm10),
                        power: false
                    });
                    this._power = false;
                }
            });
        }
    }

    turnOn() {
        if (!this._power) this._dth.write("1");
    }
    turnOff() {
        if (this._power) this._dth.write("0");
    }

    disconnect() {
        this._dth.disconnect().then(() => {
            console.log("ConnectSP_DTH: " + this._portName + " disconnected");
        });
    }

}

module.exports = ConnectSP_DTH;
/*
const port = new SerialPort('COM5', {
    autoOpen: false,
    baudRate: 9600
});

port.open(function (err) {
    if (err) {
        return console.log('Error opening port: ', err.message);
    }

    // Because there's no callback to write, write errors will be emitted on the port:
    port.write('main screen turn on');
});

// The open event is always emitted
port.on('open', function() {
    // open logic
});
port.on('data', function(data) {
    let temp = data.toString().split(",");
    let obj = {
        dustRatio: temp[0],
        temperature:temp[1],
        humidity: temp[2]
    };
    //console.log(data.toString());
    console.table(obj);
});*/
