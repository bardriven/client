/*
 * Copyright (c) 2018. OsirptPm
 * 이 저작물에는 크리에이티브 커먼즈 저작자표시-비영리-동일조건변경허락 4.0 국제 라이선스가 적용 되어 있습니다. 이 라이선스의 설명을 보고 싶으시면 http://creativecommons.org/licenses/by-nc-sa/4.0/ 을(를) 참조하세요.
 */

const AWSIoTDevice = require("./helpers/AWSIoTDevice");
const DTHShadow = require("./configs/aws/DTHShadow");

// let shadow = {
//     state: {
//         reported: {
//             pm2_5: 0,
//             pm10: 0,
//             temperature: 0,
//             humidity: 0,
//             power: false
//         },
//         desired: {
//         }
//     },
//     clientToken: ""
// };

/**
 * @extends AWSIoTDevice
 */
class Thing_DTH extends AWSIoTDevice {
    /**
     *
     * @return {{state: {reported: {pm2_5: number, pm10: number, temperature: number, humidity: number, power: boolean}, desired: {power: boolean}}, clientToken: string}}
     */
    get shadow() {
        return this._shadow;
    }

    /**
     *
     * @returns {number}
     */
    get temperature() {
        return this.shadow.state.reported.temperature;
    }
    /**
     *
     * @param {number} value
     */
    set temperature(value) {
        this.shadow.state.reported.temperature = value;
    }
    /**
     *
     * @returns {number}
     */
    get humidity() {
        return this.shadow.state.reported.humidity;
    }
    /**
     *
     * @param {number} value
     */
    set humidity(value) {
        this.shadow.state.reported.humidity = value;
    }
    /**
     *
     * @returns {number}
     */
    get pm2_5() {
        return this.shadow.state.reported.pm2_5;
    }
    /**
     *
     * @param {number} value
     */
    set pm2_5(value) {
        this.shadow.state.reported.pm2_5 = value;
    }
    /**
     *
     * @returns {number}
     */
    get pm10() {
        return this.shadow.state.reported.pm10;
    }
    /**
     *
     * @param {number} value
     */
    set pm10(value) {
        this.shadow.state.reported.pm10 = value;
    }
    /**
     *
     * @returns {boolean}
     */
    get desiredPower() {
        return this.shadow.state.desired.power;
    }
    /**
     *
     * @param {boolean} value
     */
    set desiredPower(value) {
        this.shadow.state.desired.power = value;
    }
    /**
     *
     * @returns {boolean}
     */
    get reportedPower() {
        return this.shadow.state.reported.power;
    }
    /**
     *
     * @param {boolean} value
     */
    set reportedPower(value) {
        this.shadow.state.reported.power = value;
    }

    /**
     *
     * @returns {string}
     */
    get clientToken() {
        return this.shadow.clientToken;
    }
    /**
     *
     * @param {string} value
     */
    set clientToken(value) {
        this.shadow.clientToken = value;
    }

    constructor() {
        super();

        /**
         *
         * @type {{state: {reported: {pm2_5: number, pm10: number, temperature: number, humidity: number, power: boolean}, desired: {power: boolean}}, clientToken: string}}
         * @private
         */
        this._shadow = DTHShadow;
    }
}

/**
 *
 * @type {Thing_DTH}
 */
module.exports = Thing_DTH;